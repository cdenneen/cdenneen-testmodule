require 'spec_helper_acceptance'

describe 'testmodule', unless: UNSUPPORTED_PLATFORMS.include?(fact('osfamily')) do
  # create empty hash
  puppet_env = {}
  # Set to avoid vagrant issues
  puppet_env[:FACTER_vm_type] = 'vagrant'

  # Set ap_env
  puppet_env[:FACTER_ap_env] = if ENV['ap_env']
                                 ENV['ap_env']
                               else
                                 'dev'
                               end

  it 'with testmodule' do
    pp = <<-EOS
      include testmodule
    EOS

    # Run it twice and test for idempotency
    apply_manifest(pp, environment: puppet_env, catch_failures: true, debug: true)
    # Run additional time because of IPTables clearing old Firewalld rules still
    apply_manifest(pp, environment: puppet_env, catch_failures: true, debug: true)
    apply_manifest(pp, environment: puppet_env, catch_changes: do_catch_changes, debug: true)
  end
end
