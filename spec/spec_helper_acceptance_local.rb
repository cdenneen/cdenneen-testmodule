require 'spec_helper_acceptance'

def do_catch_changes
  if default['platform'] =~ %r{centos}
    false
  else
    true
  end
end
